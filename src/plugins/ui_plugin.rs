use crate::{
    api::{spawn_weather_task, WeatherApiError, WeatherData, WeatherTask},
    plugins::Settings,
    ui_components::{weather_card, weather_plot, SelectedPlot},
};
use bevy::{prelude::*, tasks::futures_lite::future};
use bevy_egui::{
    egui::{self, vec2},
    EguiContexts,
};

#[derive(Component, Default)]
pub struct UiState {
    pub location_input: String,
    pub location: String,
    pub loading: bool,
    pub settings_open: bool,
    pub selected_plot: SelectedPlot,
}

#[derive(Component, Default)]
pub struct WeatherDataState {
    pub weather_data: Option<Result<WeatherData, WeatherApiError>>,
}

pub struct UiPlugin;

impl Plugin for UiPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, setup_ui_state)
            .add_systems(Update, (load_default_location, render_ui));
    }
}

fn setup_ui_state(mut commands: Commands) {
    commands.spawn(UiState::default());
    commands.spawn(WeatherDataState::default());
}

fn load_default_location(
    mut commands: Commands,
    mut ui_state_query: Query<&mut UiState>,
    mut weather_data_state_query: Query<&mut WeatherDataState>,
    settings_query: Query<&mut Settings>,
    current_weather_tasks: Query<(Entity, &mut WeatherTask)>,
) {
    let mut ui_state = ui_state_query.single_mut();
    let weather_data_state = weather_data_state_query.single_mut();
    let settings = settings_query.single();

    if weather_data_state.weather_data.is_none() && current_weather_tasks.iter().next().is_none() {
        ui_state.loading = true;
        ui_state.location = settings.location.clone();
        ui_state.location_input.clear();
        spawn_weather_task(&mut commands, ui_state.location.clone());
    }
}

fn render_ui(
    mut contexts: EguiContexts,
    mut commands: Commands,
    mut ui_state_query: Query<&mut UiState>,
    mut weather_data_state_query: Query<&mut WeatherDataState>,
    mut current_weather_tasks: Query<(Entity, &mut WeatherTask)>,
) {
    let mut ui_state = ui_state_query.single_mut();
    let mut weather_data_state = weather_data_state_query.single_mut();

    for (entity, mut task) in current_weather_tasks.iter_mut() {
        if let Some(response) = future::block_on(future::poll_once(&mut task.0)) {
            info!("Got Response: {:?}", response);
            weather_data_state.weather_data = Some(response);
            commands.entity(entity).despawn();
            ui_state.loading = false;
        }
    }

    let bg_color = egui::Color32::from_rgb(30, 30, 30);

    egui::TopBottomPanel::top("Bevy Weather")
        .frame(egui::Frame::none().inner_margin(10.0).fill(bg_color))
        .show(contexts.ctx_mut(), |ui| {
            ui.style_mut().spacing.indent = 16.0;
            ui.heading("Bevy Weather");
            ui.horizontal(|ui| {
                if ui.button("Settings").clicked() {
                    ui_state.settings_open = !ui_state.settings_open;
                }
                if ui.button("Home").clicked() {
                    weather_data_state.weather_data = None;
                }
                ui.label("Location: ");
                if ui
                    .text_edit_singleline(&mut ui_state.location_input)
                    .lost_focus()
                    && ui.input(|i| i.key_pressed(egui::Key::Enter))
                {
                    if !ui_state.location_input.is_empty() {
                        ui_state.loading = true;
                        ui_state.location = ui_state.location_input.clone();
                        ui_state.location_input.clear();
                        spawn_weather_task(&mut commands, ui_state.location.clone());
                    }
                }
                if ui.button("Get Weather").clicked() {
                    if !ui_state.location_input.is_empty() {
                        ui_state.loading = true;
                        ui_state.location = ui_state.location_input.clone();
                        ui_state.location_input.clear();
                        spawn_weather_task(&mut commands, ui_state.location.clone());
                    }
                }
                ui.add_space(10.0);
            });
        });

    egui::CentralPanel::default()
        .frame(egui::Frame::none().inner_margin(10.0).fill(bg_color))
        .show(contexts.ctx_mut(), |ui| {
            ui.style_mut().spacing.indent = 16.0;
            if ui_state.loading {
                ui.spinner();
            } else {
                if let Some(data) = &weather_data_state.weather_data {
                    match data {
                        Ok(data) => {
                            egui::ScrollArea::vertical().show(ui, |ui| {
                                ui.heading(format!("Current Weather for {}", data.location));
                                weather_card(ui, &data.current, &data.units);
                                ui.separator();
                                ui.horizontal(|ui| {
                                    ui.selectable_value(
                                        &mut ui_state.selected_plot,
                                        SelectedPlot::Temperature,
                                        "Temperature",
                                    );
                                    ui.selectable_value(
                                        &mut ui_state.selected_plot,
                                        SelectedPlot::Humidity,
                                        "Humidity",
                                    );
                                    ui.selectable_value(
                                        &mut ui_state.selected_plot,
                                        SelectedPlot::WindSpeed,
                                        "Windspeed",
                                    );
                                });
                                ui.add_space(16.0);
                                ui.horizontal(|ui| {
                                    weather_plot(ui, &data, ui_state.selected_plot.clone());
                                    ui.add_space(16.0);
                                });
                                ui.separator();
                                ui.heading("Forecast");
                                for hour in data.hourly.iter() {
                                    weather_card(ui, &hour, &data.units);
                                    ui.separator();
                                }
                            });
                        }
                        Err(e) => {
                            ui.style_mut().spacing.indent = 16.0;
                            ui.label(format!("{}", e));
                        }
                    }
                } else {
                    ui.style_mut().spacing.indent = 16.0;
                    ui.label("Select a location to get weather data.");
                }
            }
        });
}
