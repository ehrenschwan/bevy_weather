use crate::plugins::UiState;
use bevy::prelude::*;
use bevy_egui::{egui, EguiContexts};
use egui::{Color32, RichText};
use serde::{Deserialize, Serialize};
use std::{
    fs::{self, File},
    io::Write,
};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum SettingsError {
    #[error("Config folder '$XDG_CONFIG_HOME/bevy_weather/' is missing")]
    MissingConfigFolder,
    #[error("Config file '$XDG_CONFIG_HOME/bevy_weather/config.toml' is missing")]
    NoConfigFile,
    #[error("There was an error saving the settings.")]
    ErrorSaving,
}

#[derive(Clone, Debug, Default, Component, Serialize, Deserialize)]
pub struct Settings {
    pub location: String,
}

#[derive(Component, Default)]
pub struct SettingsState {
    pub location_input: String,
    pub saving_error: bool,
    pub saved: bool,
}

#[derive(Bundle)]
pub struct SettingsBundle {
    settings: Settings,
    settings_state: SettingsState,
}

pub struct SettingsPlugin;

impl Plugin for SettingsPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, setup_settings)
            .add_systems(Update, render_settings);
    }
}

/// Load settings from config file or use default settings
fn setup_settings(mut commands: Commands) {
    let mut settings = Settings::default();

    if let Some(parsed_settings) = read_settings() {
        settings = parsed_settings
    }

    let mut settings_state = SettingsState::default();

    settings_state.location_input = settings.location.clone();

    commands.spawn(SettingsBundle {
        settings,
        settings_state,
    });
}

fn read_settings() -> Option<Settings> {
    let xdg_dirs = xdg::BaseDirectories::with_prefix("bevy_weather").ok()?;
    let config_file = xdg_dirs.find_config_file("config.toml")?;
    let content = fs::read_to_string(config_file.clone()).ok()?;
    let settings = toml::from_str(&content).ok()?;
    info!(
        "Parsed config file: {}\nSettings: {:?}",
        config_file.display(),
        settings
    );
    Some(settings)
}

fn render_settings(
    mut contexts: EguiContexts,
    mut ui_state_query: Query<&mut UiState>,
    mut settings_query: Query<(&mut Settings, &mut SettingsState)>,
) {
    let mut ui_state = ui_state_query.single_mut();
    let (mut settings, mut settings_state) = settings_query.single_mut();

    if ui_state.settings_open {
        egui::Window::new("Settings")
            .collapsible(false)
            .show(contexts.ctx_mut(), |ui| {
                ui.horizontal(|ui| {
                    ui.label("Default Location:");
                    ui.text_edit_singleline(&mut settings_state.location_input)
                });
                if settings_state.saving_error {
                    ui.label(
                        RichText::new("There was an error saving the settings").color(Color32::RED),
                    );
                }
                if settings_state.saved {
                    ui.label(
                        RichText::new("Settings saved to $XDG_CONFIG_HOME").color(Color32::GREEN),
                    );
                }
                ui.horizontal(|ui| {
                    if ui.button("Save").clicked() {
                        settings_state.saving_error = false;
                        settings.location = settings_state.location_input.clone();
                        if save_settings(settings.clone()).is_err() {
                            settings_state.saving_error = true;
                        } else {
                            settings_state.saved = true;
                        };
                    }
                    if ui.button("Close").clicked() {
                        settings_state.saving_error = false;
                        ui_state.settings_open = false;
                        settings_state.saved = false;
                        settings_state.location_input = settings.location.clone();
                    }
                });
            });
    }
}

/// Save settings to config file
///
/// # Arguments
/// * `settings` - Settings to save
/// # Returns
/// * `Result<(), SettingsError>`
/// * `SettingsError` - Error type
fn save_settings(settings: Settings) -> Result<(), SettingsError> {
    let settings_toml = toml::to_string(&settings).map_err(|_| SettingsError::ErrorSaving)?;
    let xdg_dirs = xdg::BaseDirectories::with_prefix("bevy_weather")
        .map_err(|_| SettingsError::MissingConfigFolder)?;
    let config_path = xdg_dirs
        .place_config_file("config.toml")
        .map_err(|_| SettingsError::NoConfigFile)?;
    let mut config_file = File::create(config_path).map_err(|_| SettingsError::ErrorSaving)?;
    write!(config_file, "{}", settings_toml);
    Ok(())
}
