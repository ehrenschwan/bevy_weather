use serde::Deserialize;
use serde_this_or_that::as_f64;

#[derive(Debug, Deserialize)]
pub struct Location {
    #[serde(deserialize_with = "as_f64")]
    pub lat: f64,
    #[serde(deserialize_with = "as_f64")]
    pub lon: f64,
    pub address: Address,
}

#[derive(Debug, Deserialize)]
pub struct Address {
    pub city: Option<String>,
    pub village: Option<String>,
    pub country: String,
}
