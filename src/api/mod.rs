use bevy::log::info;

mod location_api;
mod location_data;
mod weather_api;
mod weather_data;

pub use location_data::*;
pub use weather_api::*;
pub use weather_data::*;

fn url_log(url: &str, params: &Vec<(&str, String)>) {
    info!(
        "Requesting data: {}?{}",
        url,
        params
            .iter()
            .map(|(k, v)| {
                if *k == "key" {
                    return format!("{}=*****", k);
                }
                format!("{}={}", k, v)
            })
            .collect::<Vec<String>>()
            .join("&")
    );
}
