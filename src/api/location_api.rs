use super::url_log;
use super::{location_data::Location, weather_api::WeatherApiError};
use bevy::log::info;
use reqwest::blocking::Client;

const LOCATION_API_URL: &str = "https://nominatim.openstreetmap.org";

/// Fetches location data from OpenStreetMap
///
/// # Arguments
/// * `client` - Reqwest Client
/// * `location` - Location to fetch data for
///
/// # Returns
/// * `Result<(Client, Location), WeatherApiError>`
/// * `Client` - Reqwest Client
/// * `Location` - Location data
/// * `WeatherApiError` - Error type
pub fn get_location(
    client: Client,
    location: String,
) -> Result<(Client, Location), WeatherApiError> {
    match latlon::parse(&location) {
        Ok(point) => get_location_by_latlon(client, point),
        Err(_) => get_location_by_name(client, location),
    }
}

/// Fetches location data from OpenStreetMap by latitude and longitude
///
/// # Arguments
/// * `client` - Reqwest Client
/// * `point` - Latitude and Longitude
///
/// # Returns
/// * `Result<(Client, Location), WeatherApiError>`
/// * `Client` - Reqwest Client
/// * `Location` - Location data
/// * `WeatherApiError` - Error type
fn get_location_by_latlon(
    client: Client,
    point: latlon::Point,
) -> Result<(Client, Location), WeatherApiError> {
    let lat = point.y();
    let lon = point.x();

    let params = vec![
        ("lat", lat.to_string()),
        ("lon", lon.to_string()),
        ("format", "jsonv2".to_string()),
        ("accept-language", "en".to_string()),
    ];
    let url = format!("{}/reverse", LOCATION_API_URL);

    url_log(&url, &params);

    let location = client
        .get(url)
        .query(&params)
        .send()
        .map_err(|_| WeatherApiError::FetchError)?
        .text()
        .map_err(|_| WeatherApiError::FetchError)?;

    info!("Location Response: {:?}", location);

    let mut location = serde_json::from_str::<Location>(&location).map_err(|e| {
        info!("Error: {:?}", e);
        WeatherApiError::NoLocation
    })?;

    // The API takes the nearest structure, we only use it to get the city and country
    location.lat = lat;
    location.lon = lon;

    Ok((client, location))
}

/// Fetches location data from OpenStreetMap by name
///
/// # Arguments
/// * `client` - Reqwest Client
/// * `location` - Location to fetch data for
///
/// # Returns
/// * `Result<(Client, Location), WeatherApiError>`
/// * `Client` - Reqwest Client
/// * `Location` - Location data
/// * `WeatherApiError` - Error type
fn get_location_by_name(
    client: Client,
    location: String,
) -> Result<(Client, Location), WeatherApiError> {
    let params = vec![
        ("q", location),
        ("format", "jsonv2".to_string()),
        ("addressdetails", "1".to_string()),
        ("accept-language", "en".to_string()),
    ];
    let url = format!("{}/search", LOCATION_API_URL);

    url_log(&url, &params);

    let locations = client
        .get(url)
        .query(&params)
        .send()
        .map_err(|_| WeatherApiError::FetchError)?
        .text()
        .map_err(|_| WeatherApiError::FetchError)?;

    info!("Locations Response: {:?}", locations);

    let locations = serde_json::from_str::<Vec<Location>>(&locations).map_err(|e| {
        info!("Error: {:?}", e);
        WeatherApiError::NoLocation
    })?;

    let location = locations
        .into_iter()
        .nth(0)
        .ok_or_else(|| WeatherApiError::NoLocation)?;

    Ok((client, location))
}
