use crate::api::{Location, WeatherApiError};
use chrono::{DateTime, Utc};
use iso8601_timestamp::Timestamp;
use serde::{Deserialize, Serialize};

// Response

#[derive(Debug, Serialize, Deserialize)]
pub struct WeatherResponse {
    pub current_units: Units,
    pub current: CurrentWeatherResponse,
    pub hourly: ForecastResponse,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct CurrentWeatherResponse {
    pub time: Timestamp,
    pub temperature_2m: f32,
    pub relative_humidity_2m: u8,
    pub weather_code: u8,
    pub wind_speed_10m: f32,
    pub is_day: u8,
}

/// Forecast response
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ForecastResponse {
    pub time: Vec<Timestamp>,
    pub temperature_2m: Vec<f32>,
    pub relative_humidity_2m: Vec<u8>,
    pub weather_code: Vec<u8>,
    pub wind_speed_10m: Vec<f32>,
    pub is_day: Vec<u8>,
}

// Data

#[derive(Clone, Debug)]
pub struct WeatherData {
    pub units: Units,
    pub current: Weather,
    pub hourly: Vec<Weather>,
    pub location: String,
}

impl WeatherData {
    pub fn new(units: Units, current: Weather, location: Location) -> WeatherData {
        WeatherData {
            units,
            current,
            hourly: vec![],
            location: format!(
                "{}, {}",
                if let Some(city) = location.address.city {
                    city
                } else if let Some(village) = location.address.village {
                    village
                } else {
                    format!("{:.3}° {:.3}°", location.lat, location.lon)
                },
                location.address.country
            ),
        }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Units {
    #[serde(rename = "temperature_2m")]
    pub temperature: String,
    #[serde(rename = "relative_humidity_2m")]
    pub relative_humidity: String,
    #[serde(rename = "wind_speed_10m")]
    pub wind_speed: String,
}

#[derive(Debug, Clone)]
pub struct Weather {
    pub time: DateTime<Utc>,
    pub temperature: f32,
    pub relative_humidity: u8,
    pub weather_code: u8,
    pub wind_speed: f32,
    pub is_day: u8,
}

impl Weather {
    pub fn new(
        time: Timestamp,
        temperature: f32,
        relative_humidity: u8,
        weather_code: u8,
        wind_speed: f32,
        is_day: u8,
    ) -> Result<Weather, WeatherApiError> {
        let time = DateTime::from_timestamp_millis(
            time.duration_since(Timestamp::UNIX_EPOCH)
                .whole_milliseconds()
                .try_into()
                .map_err(|_| WeatherApiError::WrongTimeFormat)?,
        )
        .ok_or_else(|| WeatherApiError::WrongTimeFormat)?;

        Ok(Weather {
            time,
            temperature,
            relative_humidity,
            weather_code,
            wind_speed,
            is_day,
        })
    }
}
