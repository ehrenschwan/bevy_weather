use super::{location_api::get_location, url_log};
use crate::api::{Location, Weather, WeatherData, WeatherResponse};
use bevy::{
    prelude::*,
    tasks::{IoTaskPool, Task},
};
use reqwest::blocking::Client;
use thiserror::Error;

const USER_AGENT: &str = concat!(env!("CARGO_PKG_NAME"), "/", env!("CARGO_PKG_VERSION"));

#[derive(Error, Debug)]
pub enum WeatherApiError {
    #[error("Location not found")]
    NoLocation,
    #[error("Weather data not found")]
    NoWeather,
    #[error("Error fetching data")]
    FetchError,
    #[error("Error converting datetimes")]
    WrongTimeFormat,
}

#[derive(Component)]
pub struct WeatherTask(pub Task<Result<WeatherData, WeatherApiError>>);

/// Spawns a new task to fetch weather data
///
/// # Arguments
/// * `commands` - Bevy Commands
/// * `location` - Location to fetch weather data for
pub fn spawn_weather_task(commands: &mut Commands, location: String) {
    let thread_pool = IoTaskPool::get();

    let task = thread_pool.spawn(async move {
        let client = Client::builder().user_agent(USER_AGENT).build();
        info!("User-Agent: {}", USER_AGENT);

        match client {
            Ok(client) => match get_location(client, location) {
                Ok((client, location)) => get_weather(client, location),
                Err(e) => Err(e),
            },
            Err(_) => Err(WeatherApiError::FetchError),
        }
    });

    commands.spawn(WeatherTask(task));
}

/// Fetches weather data from Open-Meteo
///
/// # Arguments
/// * `client` - Reqwest Client
/// * `location` - Location to fetch weather data for
///
/// # Returns
/// * `Result<WeatherData, WeatherApiError>`
/// * `WeatherData` - Weather data for the location
/// * `WeatherApiError` - Error type
fn get_weather(client: Client, location: Location) -> Result<WeatherData, WeatherApiError> {
    let variables = "temperature_2m,relative_humidity_2m,weather_code,wind_speed_10m,is_day";

    let params = vec![
        ("latitude", location.lat.to_string()),
        ("longitude", location.lon.to_string()),
        ("current", variables.to_string()),
        ("hourly", variables.to_string()),
        ("past_days", "1".to_string()),
        ("forecast_days", "2".to_string()),
        ("timezone", "auto".to_string()),
    ];

    let url = "http://api.open-meteo.com/v1/forecast";

    url_log(&url, &params);

    let weather_res = client
        .get(url)
        .query(&params)
        .send()
        .map_err(|_| WeatherApiError::FetchError)?
        .text()
        .map_err(|_| WeatherApiError::FetchError)?;

    info!("Weather Response: {:?}", weather_res);

    let weather_res = serde_json::from_str::<WeatherResponse>(&weather_res)
        .map_err(|_| WeatherApiError::NoWeather)?;

    let current = weather_res.current;
    let mut weather_data = WeatherData::new(
        weather_res.current_units,
        Weather::new(
            current.time,
            current.temperature_2m,
            current.relative_humidity_2m,
            current.weather_code,
            current.wind_speed_10m,
            current.is_day,
        )?,
        location,
    );

    for i in 0..weather_res.hourly.time.len() {
        let hourly = weather_res.hourly.clone();
        let weather = Weather::new(
            hourly.time[i],
            hourly.temperature_2m[i],
            hourly.relative_humidity_2m[i],
            hourly.weather_code[i],
            hourly.wind_speed_10m[i],
            hourly.is_day[i],
        )?;

        weather_data.hourly.push(weather);
    }

    weather_data.hourly = weather_data
        .hourly
        .into_iter()
        .filter(|h| h.time > weather_data.current.time)
        .collect();

    weather_data.hourly.truncate(24);

    Ok(weather_data)
}
