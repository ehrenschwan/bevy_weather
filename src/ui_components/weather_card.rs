use crate::api::{Units, Weather};
use bevy_egui::egui::{RichText, Ui};

/// Weather card
///
/// Displays weather data using the Egui UI.
///
/// # Arguments
/// * `ui` - Egui Ui
/// * `weather` - Weather data
/// * `units` - Units
pub fn weather_card(ui: &mut Ui, weather: &Weather, units: &Units) {
    ui.label(RichText::new(format!("{}", weather.time.format("%a, %Y.%m.%d - %H:%M"))).strong());
    ui.label(format!(
        "{}",
        get_weather_conditions(weather.weather_code, weather.is_day),
    ));
    ui.label(format!(
        "Current temperature: {}{}",
        weather.temperature, units.temperature
    ));
    ui.label(format!(
        "humidity: {}{}",
        weather.relative_humidity, units.relative_humidity
    ));
    ui.label(format!(
        "Wind speed: {}{}",
        weather.wind_speed, units.wind_speed
    ));
}

/// Gets the weather conditions from the weather code
///
/// # Arguments
/// * `weather_code` - Weather code
/// * `is_day` - Is it day
///
/// # Returns
/// * `&str` - Weather conditions
fn get_weather_conditions(weather_code: u8, is_day: u8) -> &'static str {
    match weather_code {
        0 => {
            if is_day == 1 {
                "Sunny"
            } else {
                "Clear"
            }
        }
        1 => {
            if is_day == 1 {
                "Mainly Sunny"
            } else {
                "Mainly Clear"
            }
        }
        2 => "Partly Cloudy",
        3 => "Cloudy",
        45 => "Foggy",
        48 => "Rime Fog",
        51 => "Light Drizzle",
        53 => "Drizzle",
        55 => "Heavy Drizzle",
        56 => "Light Freezing Drizzle",
        57 => "Freezing Drizzle",
        61 => "Light Rain",
        63 => "Rain",
        65 => "Heavy Rain",
        66 => "Light Freezing Rain",
        67 => "Freezing Rain",
        71 => "Light Snow",
        73 => "Snow",
        75 => "Heavy Snow",
        77 => "Snow Grains",
        80 => "Light Snow Showers",
        81 => "Snow Showers",
        82 => "Heavy Snow Showers",
        85 => "Snow Flurries",
        86 => "Light Snow Flurries",
        95 => "Thunderstorm",
        96 => "Thunderstorm with Light Rain",
        99 => "Thunderstorm with Heavy Rain",

        _ => panic!("Unknown weather code"),
    }
}
