use crate::api::WeatherData;
use bevy_egui::egui::{vec2, Ui};
use chrono::DateTime;
use egui_plot::{GridMark, Line, Plot, PlotPoints};
use std::collections::VecDeque;

#[derive(PartialEq, Clone)]
pub enum SelectedPlot {
    Temperature,
    Humidity,
    WindSpeed,
}

impl Default for SelectedPlot {
    fn default() -> Self {
        Self::Temperature
    }
}

/// Weather plot
///
/// A plot using egui_plot to display temperature, humidity, or wind speed data.
///
/// # Arguments
/// * `ui` - Egui Ui
/// * `data` - Weather data
/// * `selected_plot` - Selected plot
pub fn weather_plot(ui: &mut Ui, data: &WeatherData, selected_plot: SelectedPlot) {
    let now = data.current.time.timestamp();
    let last_hour = now - (now % 3600);

    let mut data_points = data
        .hourly
        .clone()
        .into_iter()
        .map(|h| {
            let time = h.time.timestamp() - last_hour;
            let value = match selected_plot {
                SelectedPlot::Temperature => h.temperature,
                SelectedPlot::Humidity => h.relative_humidity.into(),
                SelectedPlot::WindSpeed => h.wind_speed,
            } as f64;
            [time as f64, value]
        })
        .collect::<VecDeque<[f64; 2]>>();
    let value = match selected_plot {
        SelectedPlot::Temperature => data.current.temperature,
        SelectedPlot::Humidity => data.current.relative_humidity as f32,
        SelectedPlot::WindSpeed => data.current.wind_speed,
    };
    data_points.push_front([(now % 3600) as f64, value.into()]);

    let data_points: PlotPoints = data_points.into_iter().collect();
    let line = Line::new(data_points);

    let unit = match selected_plot {
        SelectedPlot::Temperature => data.units.temperature.clone(),
        SelectedPlot::Humidity => data.units.relative_humidity.clone(),
        SelectedPlot::WindSpeed => data.units.wind_speed.clone(),
    };
    let axis_unit = unit.clone();
    let label_unit = unit.clone();
    let mut step = -1.0;
    ui.vertical_centered(|ui| {
        Plot::new("Weather Forecast")
            .view_aspect(3.0)
            .set_margin_fraction(vec2(0.0, 0.25))
            .x_grid_spacer(move |_| {
                std::iter::from_fn(move || {
                    step += 1.0;

                    if step > 24.0 {
                        None
                    } else {
                        Some(GridMark {
                            value: 3600.0 * step,
                            step_size: 3600.0,
                        })
                    }
                })
                .collect::<Vec<GridMark>>()
            })
            .x_axis_formatter(move |mark, _, _| {
                let time = DateTime::from_timestamp(last_hour + mark.value as i64, 0)
                    .expect("created from time_stamps");
                time.format("%H:%M").to_string()
            })
            .y_axis_formatter(move |mark, _, _| format!("{}{}", mark.value, axis_unit))
            .label_formatter(move |_, point| {
                let time = DateTime::from_timestamp(last_hour + point.x as i64, 0)
                    .expect("created from time_stamps");
                format!(
                    "{:.1}{}, {}",
                    point.y,
                    label_unit,
                    time.format("%H:%M").to_string()
                )
            })
            .allow_drag(false)
            .allow_scroll(false)
            .show(ui, |plot_ui| {
                plot_ui.line(line);
            })
            .response;
    });
}
