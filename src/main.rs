#![deny(clippy::unwrap_used)]

use bevy::prelude::*;
use bevy_egui::EguiPlugin;

mod api;
mod plugins;
mod ui_components;

use plugins::{SettingsPlugin, UiPlugin};

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugins(EguiPlugin)
        .add_plugins(UiPlugin)
        .add_plugins(SettingsPlugin)
        .run();
}
