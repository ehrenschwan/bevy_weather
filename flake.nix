{
  description = "A bevy weather companion";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    crane = {
      url = "github:ipetkov/crane";
      inputs = { nixpkgs.follows = "nixpkgs"; };
    };
  };

  outputs = { self, nixpkgs, flake-utils, fenix, crane, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [ fenix.overlays.default ];

        pkgs = import nixpkgs {
          inherit system overlays;
          config.allowUnfree = true;
        };

        inherit (pkgs) lib;

        rustPkg = pkgs.fenix.fromToolchainFile {
          file = ./rust-toolchain.toml;
          sha256 = "sha256-MM2K43Kg+f83XQXT2lI7W/ZdQjLXhMUvA6eGtD+rqDY";
        };

        buildInputs = with pkgs;
          [
            vulkan-tools
            vulkan-headers
            vulkan-validation-layers
            vulkan-loader

            # wayland
            libxkbcommon
            wayland

            alsa-lib
            udev

            openssl
            openssl.dev
          ] ++ (with xorg; [ libXrandr libXcursor libXi libX11 ]);

        craneLib = crane.lib.${system}.overrideToolchain rustPkg;
        src = lib.cleanSourceWith {
          src = ./.;
          filter = path: type:
            (lib.hasInfix "assets" path)
            || (craneLib.filterCargoSources path type);
        };

        packageName = craneLib.crateNameFromCargoToml { inherit src; };

        commonArgs = {
          inherit src buildInputs;
          strictDeps = true;

          nativeBuildInputs = with pkgs; [ pkg-config makeWrapper clang lld ];
          BEVY_ASSET_PATH = "${src}/assets";
        };

        cargoArtifacts = craneLib.buildDepsOnly commonArgs;

        my-package = craneLib.buildPackage (commonArgs // {
          inherit cargoArtifacts;

          postInstall = ''
            wrapProgram $out/bin/${packageName.pname} \
              --prefix LD_LIBRARY_PATH : ${
                pkgs.lib.makeLibraryPath buildInputs
              } \
              --prefix XCURSOR_THEME : "Adwaita"
          '';
        });
      in {
        checks = { inherit my-package; };
        packages.default = my-package;
        devShells.default = craneLib.devShell {
          checks = self.checks.${system};

          inputsFrom = [ my-package ];

          RUST_BACKTRACE = "1";
          LD_LIBRARY_PATH = pkgs.lib.makeLibraryPath buildInputs;
        };
      });
}
