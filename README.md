# Bevy Weather

This is an example program for an application. It uses the <https://open-meteo.com/> api for the weather data and [nominatim](https://nominatim.org/release-docs/develop/) for the location data. These are both free apis without keys, this makes usage very easy. But also comes with some issues like request limiting. So if there are any problems with that please let me know. It is build how I would usually build a program with all the tools I would normally use.

## Attribution
- [nominatim](https://nominatim.org/release-docs/develop/) is a service by [OpenStreatMap](openstreetmap.org/copyright), it's data is available under the Open Database License.

- [Open-Meteo](https://open-meteo.com/) is an open-source weather API, it is Licensed under the AGPLv3 licence. The data provided by Open-Meteo is licensed under the [Attribution 4.0 Internation (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/).

## Architecture

### `api`

The api is for loading the weather as well as location data. It consists of the `weather_api`, and `location_api`, along with their data.

#### `weather_api`

This is the main entry point of the api. It's main purpose is spawning a `WeatherTask` which is used to request the weather data. This is done by using an `IoTaskPool`. This task first requests the location data, which is done by the [`location_api`](#`location_api`). After that it requests the Weather data with the given longitude and latitute. The request is done by using reqwests blocking modules client. Normally this would halt the program until finished, but in this case this isn't a problem as this is run inside of an [bevy task](https://docs.rs/bevy/latest/bevy/tasks/index.html), which is run on another thread. The response is then deserialized using [serde_json](https://docs.rs/serde_json/latest/serde_json/). Unfortunately the response isn't well formed to be used as is in the application, so the data is processed into the `WeatherData` struct. For the full 24 hours forecast, the request, requests one day backwards and two days forward and then filter and truncate the results.

#### `location_api`

This module contains the functions for requesting location information or longitude and latitude when searching by name. This reuses the client from the `weather_api`. It works by first trying to parse the input as a `Point` from the [latlon](https://docs.rs/latlon/latest/latlon/index.html) crate. If it succeeds it then requests the information for that location based on that with a reverse search. Otherwise it requests the data using the normal forward search of the [nominatim](https://nominatim.org/release-docs/develop/) api. The response is also deserialized using [serde_json](https://docs.rs/serde_json/latest/serde_json/), however this time the data is already usable, so it is only trimmed to the first result in case of the search by name.

### `plugins`
 
These are the bevy plugins used in the app.

#### `ui_plugin`

This is the main ui plugin. It contains the whole ui except for the settings. It defines two bevy components which hold the bulk of the application state. And it adds two system to the program. First is the `setup_ui_state`. This just spawns the two components with their default values. It is run during the `Startup` schedule.

##### `load_default_location`
This system loads the data for the default location whenever WeatherData is `None`, so this is usually after starting the program or after pressing the `Home` button, which simply just sets WeatherData to `None` to trigger reloading in this system.

##### `render_ui`

This system is responsible for rendering the ui as well as handle the user interaction. It inserts a `egui::TopBottomPanel::top()` which contains the search as well as buttons for the settings and the "home" screen, which is just the ui loaded with the location set in the settings. It also contains the `Get Weather` button which calls the `spawn_weather_task` function to start the data fetching. The `egui::CentralPanel` holds the ui for displaying the data. It first shows location name with country and the current weather using the [`weather_card`](#`weather_card`) ui-component. After that it show's a plot which can either show `Temperature`, `Humidity`, or `Windspeed` for the next 24 hours. This is accomplished by [`egui_plot`](https://docs.rs/egui_plot/0.26.2/egui_plot/), this has to be used due to the egui version in [`bevy_egui`](https://docs.rs/bevy_egui/latest/bevy_egui/). After that it shows a list for the weather forecast of each hour for the next 24 hours. As well as being responsible for starting the fetching of the data, the system is also responsible for polling the task. It receives the task using a bevy query for the `WeatherTask` bevy component and that polls it once. As this happens in the `Update` schedule this happens once per frame. This way the request doesn't block the ui. If it receives a response it sets the `WeatherData` Component to the received value.

#### `settings_plugin`

This plugin renders the settings window. It also loads and saves the settings. It has two components, which hold the settings itself and ui state, inside of a bevy bundle. It then adds two systems. `setup_settings` which is run during `Startup`. And `render_settings`, which also saves the settings. The settings are loaded from a [file](## Configuration) located in the config folder, which is done by deserializing using [`toml`](https://docs.rs/toml/latest/toml/). It shows the settings window, which are only rendered when `settings_open` on the `UiState` is set to `true`. It always show's the current settings, as the state is always kept up to date after saving. After changing settings the settings can be saved to the [`config file`](#Configuration). This is also done by using [`toml`](https://docs.rs/toml/latest/toml/).

### `ui_components`

This plugin contains two ui components. 

#### `weather_plot`

The first is the plot, which uses [`egui_plot`](https://docs.rs/egui_plot/0.26.2/egui_plot/). For the plot the hourly values are processed into `Line`s. The api request also delivers units, so the axis and the label can also be formatted correctly. Which data is shown depends on the `selectedPlot` enum. There are also custom grid spacer included to have the plot to show correctly.

#### `weather_card`

The `weather_card` is responsible for displaying the weather data. It formats each field of the `WeatherData` with it's corresponding units. It also has a function for getting weather conditions from the `weather_code` and wether if it's day or night. The text comes from this [gist](https://gist.github.com/stellasphere/9490c195ed2b53c707087c8c2db4ec0c).

## Error handling

For delivering the user a good Error message, in case of such. This app uses the [`thiserror`](https://docs.rs/thiserror/latest/thiserror/) and it's macros to define Error enums for the weather api and the settings plugin. These define human readable error codes and show them in their respective places in case of an error.

## Nix

This project is configured using a nix flake. It uses [`fenix`](https://github.com/nix-community/fenix) for installing the rust toolchain, and [`crane`](https://github.com/ipetkov/crane) for building the project. Fenix installs the too-chain profile and components on the right channel specified in the `rust-toolchain.toml`. Crane is then used to build the project using the installed toolchain. Crane is used for example to get the name out of the `Cargo.toml` as wel as other information to build the package. Crane then builds the package using some features like building the dependencies only first, which could be cached in CI/CD. Crane is also responsible for the `devShell` which get's all the inputs from the package, so the environment is the same as for building. It also adds two ENV-Variables for development. The shell can be entered by using:

```sh
nix develop
```

> Note: for this to work, nix has to be installed and the `flakes` as well as the `nix-command` features has to be enabled

or when installed by allowing [`direnv`](https://direnv.net/) to run:

```sh
direnv allow
```

## Building the project

On nix the package can be build by running:

```sh
nix build
```

which then builds the project to the `/result` folder. It can also just be run by using:

```sh
nix run
```

On non nix systems the normal cargo commands can be used.

> Note: There might be some extra dependencies that have to be installed manually, for these see the buildInputs in the flake.

## Configuration

Bevy Weather can be configured by placing a `config.toml` file into the `bevy_weather` folder in the `$XDG_CONFIG_HOME` (which is usually `~/.config/bevy_weather/config.toml` on most linux systems). With it the default or home location can be defined.

```toml
# ~/.config/bevy_weather/config.toml
location = "Berlin"
```

## Known issues

- __Long compile times__,
  The compile takes very long, this is mostly due to bevy and its list of dependencies. This can probably be somewhat reduced but the focus was more on features than on optimizing. This project uses nightly and the nightly compile so it uses the new [multi threaded compiler feature](https://gitlab.com/ehrenschwan/bevy_weather/-/blob/main/.cargo/config.toml?ref_type=heads#L6). The `dynamic_linking` feature could be use but is not easily possible on NixOS. 
- __No condition images__,
  The weather only shows text for the condition and not an image as usual for weather apps. There has been work on this on [feat/condition-images](https://gitlab.com/ehrenschwan/bevy_weather/-/tree/feat/condition-images?ref_type=heads) but is a bit harder due how bevy and egui behave together.
- __Extra processing of the weather data__,
  The weather api is very simple to use, but the formatting of the data isn't ideal so extra processing steps have to be done. This could be handled by using a better api, which would come at other costs.
- __Usage of `egui_plot`__,
  The plot is included in `egui` in the `0.27^` release, however `bevy_egui` only re-exports `0.26.2`. This version does not include the graph, so the additional package needs to be included. `0.27^` seems like a very recent release, so `bevy_egui` could adapt in the near future.
- __No clipboard support__,
  The clipboard does not work on wayland, it might work on X11, but I haven't had time to test that yet.
- __rust-analyzer__,
  The rust-analyzer seems to hang at `building proc-macros: bevy_weather`. For me this seem's to be somewhat related to this [issue](https://github.com/rust-lang/rust-analyzer/issues/16614) even though this should be fixed. So this could also be a problem with my editor configuration.

## [`tokei`](https://github.com/XAMPPRocky/tokei)

```
===============================================================================
 Language            Files        Lines         Code     Comments       Blanks
===============================================================================
 Nix                     1           95           76            1           18
 TOML                    2           32           27            1            4
-------------------------------------------------------------------------------
 Markdown                1          121            0           75           46
 |- TOML                 1            2            1            1            0
 (Total)                            123            1           76           46
-------------------------------------------------------------------------------
 Rust                   12          864          766            3           95
 |- Markdown             6           81            0           65           16
 (Total)                            945          766           68          111
===============================================================================
 Total                  16         1112          869           80          163
===============================================================================
```

## Resources and Tools used
- all the docs.rs docs
- <https://neovim.io/>
- <https://github.com/mvlabat/bevy_egui_web_showcase>
- <https://gist.github.com/stellasphere/9490c195ed2b53c707087c8c2db4ec0c>
- <https://sourcegraph.com/cody/chat> for extracting and analyzing the weather_codes json on the images feature branch
- <https://whoisryosuke.com/blog/2023/getting-started-with-egui-in-rust>
